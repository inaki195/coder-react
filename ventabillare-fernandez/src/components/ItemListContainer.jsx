import React, { useState } from 'react';
import ItemCount from './ItemCount';
export default function ItemListContainer({message}) {
  let maximo=10;
  let minimo = 0;
  const[x,setX] = useState(0);
  function onAdd(items) {
    if (x===0){
      
    }else{
      alert("has comprado "+items+ " items")
    }
  }
  function changeValue(accion){
   if(accion==="suma"){  
    x<maximo ? setX(x+1):console.log("error")
   }
    if(accion==="resta"){
      console.log("resta")
      x>minimo ? setX(x-1):console.log("error")

    }
  
  }
    return (
        <>
          <div class="card-body">
            <h5 class="card-title">message to send by props</h5>
            <p class="card-text">{message}</p>
            <a href="https://en.reactjs.org/docs/getting-started.html" class="btn btn-primary">learn more react</a>
          </div>
          <ItemCount stock={maximo} changeValue={changeValue} add={onAdd}  cantidad={x} />
        </>
    );
}