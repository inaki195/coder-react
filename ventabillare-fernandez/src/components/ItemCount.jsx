import React from "react";

export default function ItemCount({stock,changeValue,add,cantidad}) {
  return (
    <>
         <h3>limite {stock}</h3>
      <div class="btn-group" role="group" aria-label="...">
     
          
        <button type="button" class="btn btn-default" onClick={()=>changeValue("resta")} >
          -
        </button>
        <span class="label label-info">{cantidad}</span>
        <button type="button" class="btn btn-default " onClick={()=>changeValue("suma")} >
          +
        </button>

        <br></br>
        <button type="button" class="btn btn-primary"  onClick={()=>add(cantidad)} >add to cart</button>
      </div>
    </>
  );
}
